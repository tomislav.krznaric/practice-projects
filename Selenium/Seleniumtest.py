from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from webdriver_manager.chrome import ChromeDriverManager

print("Test poceo")

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.maximize_window()
driver.get("https://www.google.com/")
driver.find_element_by_id("L2AGLb").click()

time.sleep(2)

driver.find_element_by_name("q").send_keys("javapoint")

time.sleep(2)

driver.find_element_by_name("btnK").send_keys(Keys.ENTER)

time.sleep(2)

driver.close()

print("Test gotov")
