import random
import words
from vjesalo import vjesalo

word = random.choice(words.odgovori)
word = word.upper()

allowed_errors = 7
guesses = []
done = False

while not done:
    for letter in word:
        if letter.upper() in guesses:
            print(letter, end= " ")
        else:
            print("_", end=" ")
    print("")

    answer = "Broj pokusaja jos: " + str(allowed_errors) + "\n" + vjesalo.get(allowed_errors)
    print(answer)

    guess = input("Probaj pogoditi dalje: ")
    print("\n"*50)
    guesses.append(guess.upper())
    if guess.upper() not in word.upper():
        allowed_errors -= 1
        if allowed_errors == 0:
            break


    done = True
    for letter in word:
        if letter.upper() not in guesses:
            done = False

if done:
    print(f"Bravo, pogodio si {word}. Cestitam pobjedio si :D")

else:
    print("Zao mi je, izgubio si :(")
    print(" ")
    print("Tocan odgovor je " + (word))
    print(vjesalo.get(allowed_errors))