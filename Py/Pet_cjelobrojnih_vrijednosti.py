
try:
    number1 = int(input("Da bi znao jeli taj broj paran ili neparan, molim te napisi broj za koji zelis doznati njegovo stanje: "))
    number2 = int(input("Posto zadatak trazi da se napravi cak PET cijelobrojnih zadataka, onda molim te upisi drugi broj kojem zelis projeriti njegovo stanje: "))
    number3 = int(input("Sada si na trecemu, znas sta treba: "))
    number4 = int(input("Sve vec jasno, samo nemoj zezati i stavljati decimale, traze se cijelobrojni brojovi. Molim te napisi cetvrti broj: "))
    number5 = int(input("I dosli smo do posljenjega, molim te upisi i peti broj da doznas kakvi su brojevi koje si upisala: "))
except: print("Ovo je zadatak sa cijelokupnim brojevima, molim te nemoj se zezati i pisati slova ili decimalne brojeve nego potrebne cijelokupne brojeve, hvala!")
try:
    if (number1 % 2) == 0:
        print("Tvoj broj " + (str(number1)) + " je paran broj!")
    else:
        print("Tvoj broj " + (str(number1)) + " je neparan broj! ")

    if (number2 % 2) == 0:
        print("Broj " + (str(number2)) + " kojem si htjela vidjeti stanje je paran broj!")
    else:
        print("Broj " + (str(number2)) + " kojem si htjela vidjeti stanje je neparan broj! ")

    if (number3 % 2) == 0:
        print("Oke, broj " + (str(number3)) + " je paran broj!")
    else:
        print("Oke,  broj " + (str(number3)) + "je neparan broj! ")

    if (number4 % 2) == 0:
        print("Mislim da svi znamo da je  " + (str(number4)) + " paran broj!")
    else:
        print("Mislim da svi znamo da je  " + (str(number4)) + " neparan broj! ")

    if (number5 % 2) == 0:
        print("I eto, u iznenadenju otkrivamo da je  " + (str(number5)) + " paran broj!")
    else:
        print("I eto, u iznenadenju otkrivamo da je  " + (str(number5)) + " neparan broj! ")

except: print("Ovo je zadatak sa cijelokupnim brojevima, molim te nemoj se zezati i pisati slova ili decimalne brojeve nego potrebne cijelokupne brojeve, hvala!")