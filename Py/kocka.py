import random
number = [1,2,3,4,5,6,7,8,9]


def pogodi():

    odgovor = random.choice(number)
    bpokusaji = 5
    pobjeda = True
    while pobjeda == True:
        pokusaj = int(input("Probaj pogoditi broj koji je odabrao komp: "))
        if pokusaj == odgovor:
            pobjeda = False
        elif bpokusaji == 0:
            print("Zao mi je, ispucao si pokusaje :( ")
            break
        else:
            print("Nisi pogodio, probaj opet")
            bpokusaji = bpokusaji - 1
    if pobjeda == False:
        print("Cestitam pogodio si broj!")

pogodi()